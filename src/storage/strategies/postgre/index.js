class Postgre {
  constructor() {
    const config = require('./dbconfig.json');
    const { Client } = require('pg');

    this.connectionString =
      'postgres://' +
      config.db_user_name +
      ':' +
      config.db_password +
      '@' +
      config.ip +
      ':' +
      config.port +
      '/' +
      config.db_name;
    this.client = new Client({
      connectionString: this.connectionString,
    });
    this.connectToDB();
  }

  connectToDB() {
    return new Promise((resolve, reject) =>
      this.client.connect((err) => {
        if (err) {
          //reject(console.error('connection error', err));
          reject(err);
        } else {
          resolve(true);
        }
      })
    );
  }

  connectionDBEnd() {
    this.client.end();
  }

  getOneWithParameters(string_query, params) {
    return new Promise((resolve, reject) => {
      this.client.query(string_query, params, (err, res) => {
        if (err) {
          reject(err);
        } else {
          resolve(res.rows[0]);
        }
      });
    });
  }

  getAmountWithParameters(string_query, params) {
    return new Promise((resolve, reject) => {
      this.client.query(string_query, params, (err, res) => {
        if (err) {
          reject(err);
        } else {
          resolve(res.rows);
        }
      });
    });
  }

  getAll(string_query) {
    return new Promise((resolve, reject) => {
      this.client.query(string_query, (err, res) => {
        if (err) {
          reject(err);
        } else {
          resolve(res.rows);
        }
      });
    });
  }

  changeObject(query_string, params) {
    return new Promise((resolve, reject) => {
      this.client.query(query_string, params, (err) => {
        if (err) {
          reject(err);
        } else {
          resolve('success');
        }
      });
    });
  }

  async getAvailableBooks(credentials) {
    const books = await this.getAll(`SELECT * FROM tester WHERE tester.status='available'`);
    return books;
  }

  async getBookByTitle(credentials) {
    const book = await this.getOneWithParameters(`SELECT * FROM tester WHERE tester.title=($1)`, [credentials.title]);
    return book;
  }

  async getUserById(credentials) {
    const user = await this.getOneWithParameters(`SELECT * FROM bookworm WHERE bookworm.id=($1)`, [credentials.id]);
    return user;
  }

  async createUser(credentials) {
    const result = await this.changeObject(
      `INSERT INTO bookworm (first_name, last_name, email, phone, login, password) 
      VALUES ( ($1), ($2), ($3), ($4), ($5), ($6) )`,
      [
        credentials.first_name,
        credentials.last_name,
        credentials.email,
        credentials.phone,
        credentials.login,
        credentials.password
      ]
    );
    return result;
  }

  async saveBook(credentials) {
    const result = await this.changeObject(
      `INSERT INTO tester (title, isbn, status, author, location, holder_id, owner_id) 
      VALUES ( ($1), ($2), ($3), ($4), ($5), ($6), ($7) )`,
      [
        credentials.title,
        credentials.isbn,
        credentials.status,
        credentials.author,
        credentials.location,
        credentials.holder_id,
        credentials.owner_id
      ]
    );
    return result;
  }

  async updateBook(credentials) {
    await this.changeObject(
      `UPDATE tester SET 
      title=(CASE WHEN ($2)::varchar IS NOT NULL THEN ($2)::varchar ELSE title END), 
      isbn=(CASE WHEN ($3)::varchar IS NOT NULL THEN ($3)::varchar ELSE isbn END),
      status=(CASE WHEN ($4)::varchar IS NOT NULL THEN ($4)::varchar ELSE status END), 
      author=(CASE WHEN ($5)::varchar IS NOT NULL THEN ($5)::varchar ELSE author END), 
      location=(CASE WHEN ($6)::varchar IS NOT NULL THEN ($6)::varchar ELSE location END),  
      holder_id=(CASE WHEN ($7)::integer IS NOT NULL THEN ($7)::integer ELSE holder_id END), 
      owner_id=(CASE WHEN ($8)::integer IS NOT NULL THEN ($8)::integer ELSE owner_id END)
      WHERE tester.id=($1)`,
      [
        credentials.id, 
        credentials.title,
        credentials.isbn,
        credentials.status,
        credentials.author,
        credentials.location,
        credentials.holder_id,
        credentials.owner_id
      ]
    );
    const result = await this.getOneWithParameters(
      'SELECT * FROM tester WHERE tester.id=($1)',
      [credentials.id]
    );
    return result;
  }

  async getBooksByUserNStatus(credentials){
    console.log(...credentials.statuses);
    const result = await this.getAmountWithParameters(
      `SELECT test1.status, jsonb_agg(test2) as books FROM tester test1 
      LEFT JOIN (SELECT * FROM tester WHERE tester.tmp_holder = ($1)) test2
      ON test1.id=test2.id 
      WHERE test1.tmp_holder = ($1) AND test1.status IN (SELECT * FROM unnest(($2)::text[])) 
      GROUP BY test1.status
      UNION
      SELECT 'available', jsonb_agg(test4) as books FROM tester test3 
      LEFT JOIN (SELECT * FROM tester) test4
      ON test3.id=test4.id 
      WHERE test3.status = 'available'
      GROUP BY test3.status`,
      [
        credentials.id, 
        credentials.statuses
      ]
    );
    console.log(result); 
    return result;
  }
}

module.exports = Postgre