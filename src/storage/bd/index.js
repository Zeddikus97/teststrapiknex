import { MapStore, ReddisStore, PostgreStore } from '../strategies';

class BdStore {
  constructor(store) {
    this.storages = {
      Postgre: new PostgreStore('./dbconfig.json'),
    };
    this.store = this.storages[store];
  }
  getAllFromTable = async (credentials) => {
    try {
      const result = await this.store.getAllFromTable(credentials);
      return { status: 0, message: result };
    } catch (e) {
      return { status: 1, message: e.message };
    }
  };
  findUser = async (credentials) => {
    try {
      console.log(credentials);
      const result = await this.store.findUser(credentials);
      console.log(result);
      return {
        status: 0,
        message: result,
      };
    } catch (e) {
      console.log(e);
      return { status: 1, message: e.message };
    }
  };
  saveUser = async (credentials) => {
    try {
      const result = await this.store.saveUser(credentials);
      return { status: 0, message: result };
    } catch (e) {
      return { status: 1, message: e.message };
    }
  };
  saveModule = async (credentials) => {
    try {
      const result = await this.store.saveModule(credentials);
      return { status: 0, message: result };
    } catch (e) {
      return { status: 1, message: e.message };
    }
  };
  saveOauth = async (credentials) => {
    try {
      const result = await this.store.saveOauth(credentials);
      return { status: 0, message: result };
    } catch (e) {
      return { status: 1, message: e.message };
    }
  };
  saveToken = async (credentials) => {
    try {
      const result = await this.store.saveToken(credentials);
      return { status: 0, message: result };
    } catch (e) {
      return { status: 1, message: e.message };
    }
  };
  deleteModule = async (credentials) => {
    try {
      const result = await this.store.deleteModule(credentials);
      return { status: 0, message: result };
    } catch (e) {
      return { status: 1, message: e.message };
    }
  };
  changeModule = async (credentials) => {
    try {
      const result = await this.store.changeModule(credentials);
      return { status: 0, message: result };
    } catch (e) {
      return { status: 1, message: e.message };
    }
  };
  getCoursesWithModules = async () => {
    try {
      const result = await this.store.getCoursesWithModules();
      return { status: 0, message: result };
    } catch (e) {
      return { status: 1, message: e.message };
    }
  };
  getTest = async (credentials) => {
    try {
      const result = await this.store.getTest(credentials);
      return { status: 0, message: result };
    } catch (e) {
      return { status: 1, message: e.message };
    }
  };
  saveTopic = async (credentials) => {
    try {
      const result = await this.store.saveTopic(credentials);
      return { status: 0, message: result };
    } catch (e) {
      return { status: 1, message: e.message };
    }
  };
  saveContent = async (credentials) => {
    try {
      const result = await this.store.saveContent(credentials);
      return { status: 0, message: result };
    } catch (e) {
      return { status: 1, message: e.message };
    }
  };
  deleteTopic = async (credentials) => {
    try {
      const result = await this.store.deleteTopic(credentials);
      return { status: 0, message: result };
    } catch (e) {
      return { status: 1, message: e.message };
    }
  };
  deleteContent = async (credentials) => {
    try {
      const result = await this.store.deleteContent(credentials);
      return { status: 0, message: result };
    } catch (e) {
      return { status: 1, message: e.message };
    }
  };
  changeTopicModule = async (credentials) => {
    try {
      const result = await this.store.changeTopicModule(credentials);
      return { status: 0, message: result };
    } catch (e) {
      return { status: 1, message: e.message };
    }
  };
  verifyToken = async (token) => {
    try {
      const result = await this.store.verifyToken(token);
      return { status: 0, message: result };
    } catch (e) {
      return { status: 1, message: e.message };
    }
  };
  storeToken = async (token, user) => {
    try {
      return await this.store.storeToken(token, user);
    } catch (e) {
      return { status: 1, message: e.message };
    }
  };
  deleteToken = async (token) => {
    try {
      return await this.store.deleteToken(token);
    } catch (e) {
      return { status: 1, message: e.message };
    }
  };
  getToken = async (key) => {
    try {
      return await this.store.getToken(key);
    } catch (e) {
      return { status: 1, message: e.message };
    }
  };
  getModules = async () => {
    try {
      const result = await this.store.getModules();
      return { status: 0, message: result };
    } catch (e) {
      return { status: 1, message: e.message };
    }
  };
  getModulesByUser = async (credentials) => {
    try {
      const result = await this.store.getModulesByUser(credentials);
      return { status: 0, message: result };
    } catch (e) {
      return { status: 1, message: e.message };
    }
  };
  getTopicsByModule = async (credentials) => {
    try {
      const result = await this.store.getTopicsByModule(credentials);
      return { status: 0, message: result };
    } catch (e) {
      return { status: 1, message: e.message };
    }
  };
  viewTopic = async (credentials) => {
    try {
      const result = await this.store.viewTopic(credentials);
      return { status: 0, message: result };
    } catch (e) {
      return { status: 1, message: e };
    }
  };
  getAllViewed = async (credentials) => {
    try {
      const result = await this.store.getAllViewed(credentials);
      return { status: 0, message: result };
    } catch (e) {
      return { status: 1, message: e.message };
    }
  };
  getAllowedTopics = async () => {
    try {
    } catch (e) {
      return { status: 1, message: e.message };
    }
  };
  allowModule = async (credentials) => {
    try {
      const result = await this.store.allowModule(credentials);
      return { status: 0, message: result };
    } catch (e) {
      console.log(e);
      return { status: 1, message: e };
    }
  };
  saveTransaction = async (credentials) => {
    try {
      const result = await this.store.saveTransaction(credentials);
      return { status: 0, message: result };
    } catch (e) {
      console.log(e);
      return { status: 1, message: e };
    }
  };
  getTransactions = async () => {
    try {
      const result = await this.store.getTransactions();

      return { status: 0, message: result };
    } catch (e) {
      console.log(e);
      return { status: 1, message: e };
    }
  };
  getVideo = async (credentials) => {
    try {
      const result = await this.store.getVideo(credentials);
      return { status: 0, message: result };
    } catch (e) {
      console.log(e);
      return { status: 1, message: e };
    }
  };
}

export default BdStore;
