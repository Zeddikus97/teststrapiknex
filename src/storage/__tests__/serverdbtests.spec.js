describe('testing storage:bd', () => {
	//const TDB = new TestDBHandler();
	const Postgre = require("../strategies/postgre/index.js");
	const bd = new Postgre();

	test('test getting books', async () => {
		const result = await bd.getAvailableBooks();
		console.log(result);
		expect(result[0].author).toBe('Andrzej Sapkowski');
	}, 10000);

	test('test update book', async () => {
		const result = await bd.updateBook({
			id:1,
			author:"Andrzej",
			status:'owned',
            location:"(53.91, 27.47)",
            owner_id:5
		});
		console.log(result);
		expect(result.author).toBe('Andrzej');
		expect(result.status).toBe('owned');
		expect(result.location).toBe('(53.91, 27.47)');
		expect(result.owner_id).toBe(5);
	}, 10000);

	test('test saving/getting bookworm', async () => {
		await bd.createUser({
            first_name:"al",
            last_name:"pal",
            login:"nagibator228",
            phone:"+375239127423",
            password:"87651234",
            email:"alex.ditze231@gmail.com"
		});
		const result = await bd.getUserById({id:3});
		console.log(result);
		expect(result.first_name).toBe('al');
		bd.connectionDBEnd();
	}, 10000);

	/*
	test('test getting books by statuses', async () => {
		const result = await bd.getBooksByUserNStatus({
            id:2,
            statuses:['active','owned']
		});
		console.log(result);
		console.log(result[0].books);
		expect(result[0].books.length).toBe(2);
	}, 10000);*/
});
