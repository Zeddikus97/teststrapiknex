
exports.up = function(knex) {
    return knex.schema
    .createTable('phone', table => {
        table
            .increments('id')
            .primary()
            .notNullable();
        table.string('number').notNullable();
        table.timestamp('created_at').defaultTo(knex.fn.now());
        table.timestamp('updated_at').defaultTo(knex.fn.now());
    })
    .createTable('bookworm', table => {
        table
            .increments('id')
            .primary()
            .notNullable();
        table.string('first_name');
        table.string('last_name');
        table.string('email');
        table.string('phone');
        table.string('login');
        table.string('password');
        table.timestamp('created_at').defaultTo(knex.fn.now());
        table.timestamp('updated_at').defaultTo(knex.fn.now());
    })
    .createTable('tester', table => {
        table
            .increments('id')
            .primary()
            .notNullable();
        table.string('title');
        table.string('isbn');
        table.string('status').defaultTo('available');
        table.string('author');
        table.string('location');
        table
            .integer('holder_id')
            .references('id')
            .inTable('phone');
        table
            .integer('owner_id')
            .references('id')
            .inTable('phone');
        table
            .integer('tmp_holder')
            .references('id')
            .inTable('bookworm');
        table.timestamp('created_at').defaultTo(knex.fn.now());
        table.timestamp('updated_at').defaultTo(knex.fn.now());
    })
};

exports.down = function(knex) {
    return knex.schema
    .dropTableIfExists('tester')
    .dropTableIfExists('bookworm')
    .dropTableIfExists('phone')
};
