const json = require('../json_data/data.json');

exports.seed = function(knex) {
  // Deletes ALL existing entries
  
  return knex('tester')
    .del()
    .then(() => {
      return knex('bookworm').del();
    })
    .then(() => {
      return knex('phone').del();
    })
    .then(function() {
      // Inserts seed entries
      return knex('phone')
        .insert(json['phone'])
        .then(function() {
          return knex('bookworm').insert(json['bookworm']);
        })
        .then(function() {
          return knex('tester').insert(json['tester']);
        })
        .then( async() => {
          await knex.raw('SELECT setval(\'bookworm_id_seq\', (SELECT MAX(id) FROM bookworm));');
          await knex.raw('SELECT setval(\'phone_id_seq\', (SELECT MAX(id) FROM phone));');
          await knex.raw('SELECT setval(\'tester_id_seq\', (SELECT MAX(id) FROM tester));');
        });
  });
};